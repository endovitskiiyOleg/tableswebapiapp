﻿angular.module('myModule', ['ngAnimate', 'ui.bootstrap']);
angular.module('myModule').controller('ModalDemoCtrl', function ($scope, $uibModal, $log, employeeService) {
    
    $scope.res;
    employeeService.getEmployees().then(function (result) {
        result.data.forEach(function (e) {
            $scope.res = e;
            console.log($scope.res);
        });
    });
    $scope.items = ['item1', 'item2', 'item3'];
    $scope.newField;

    $scope.animationsEnabled = true;

    $scope.open = function (size) {

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: "myModalContent.html",
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                newField: function(){
                    return $scope.newField;
                },
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem, field) {
            //$scope.selected = selectedItem;
            //$scope.field = field;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };

});


angular.module('myModule').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, items, newField) {

    $scope.newField = newField;

    $scope.items = items;
    $scope.selected = {
        item: $scope.items[0]
    };

    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item, $scope.newField);
        if ($scope.newField && (('name' in $scope.newField) && ('type' in $scope.newField) && ('value' in $scope.newField))) {
            var type = getInputType($scope.newField.type);
            var checked = $('#multi').prop('checked');
            var multival = checked ? '&nbsp;&nbsp;&nbsp;<img src="/Content/mark.png">' : '';
            var str = $scope.newField.type == 'string' ? '&nbsp;&nbsp;&nbsp;<img src="/Content/Tree Structure-64.png">' : '';
            var tempTemplate =
            '<div class="row">' +
                '<div class="col-xs-7"><span>' + $scope.newField.name + '</span></div><div class="col-xs-3"><span>' + type + '</span>' + str + multival + '</div><div class="col-xs-2"><button class="btn btn-xs edit"><i class="glyphicon glyphicon-pencil"></i></button><button class="btn btn-xs remove"><i class="glyphicon glyphicon-remove"></i></button></div>' +
            '</div>';

            var addHeight = actualHeight + 21;
            $('.content .row:last').after(tempTemplate);
            el1.resize(actualWidth, addHeight);
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});