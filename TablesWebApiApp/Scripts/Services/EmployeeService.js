﻿angular.module('myModule').factory('employeeService', [
    '$http',
    function ($http) {
        return {
            getEmployees: function () {
                var sendData = {};
                return $http({
                    method: 'POST',
                    url: '/api/employee/getEmployees',
                    cache: false,
                    data: sendData
                }).error(function (error) {
                    if (error == null)
                        return null;
                    console.error(error.message + " " + error.exceptionMessage);
                    throw error;
                }).success(function () {
                    console.log('success');
                });
            }
        }
    }
]);