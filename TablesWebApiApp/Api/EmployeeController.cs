﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using TablesApp.Models;
using Newtonsoft.Json.Linq;

namespace TablesApp.Api
{
    public class EmployeeController : ApiController
    {
        [HttpPost]
        public IEnumerable<object> GetEmployees()
        {
            List<object> emp = new List<object>();
            using (EmployeeContext db = new EmployeeContext())
            {
                Employee e1 = new Employee {
                    Id = 1,
                    FirstName ="Ivan",
                    LastName ="Ivanov",
                    MiddleName ="Vladimirovich",
                    Email ="ivan@mail.com",
                    IsManager =true,
                    Phone ="543534",
                    Salary =4233
                };
                emp.Add(e1);
            }
                return emp;
        }
    }
}